package com.easyplayer.core.base

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.orhanobut.logger.Logger

abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout())
        Logger.i("Hello this is logger message.")
    }

    @LayoutRes abstract fun layout(): Int

    /**
     * Detach and reattach current fragment
     */
    fun restartFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.detach(fragment)
        fragmentTransaction.attach(fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }

    /**
     * Replace current fragment with the new one
     */
    fun changeFragment(@IdRes container: Int, fragment: Fragment, addToBackStack: Boolean, clearTop: Boolean) {
        if (clearTop) {
            supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(container, fragment, fragment.tag)
        if (addToBackStack)
            fragmentTransaction.addToBackStack(fragment.tag)
        fragmentTransaction.commitAllowingStateLoss()
    }

    /**
     * Handle onBackPressed for fragment stack
     */
    override fun onBackPressed() {
        val fragmentList = supportFragmentManager.fragments
        var handled = false
        for (fragment in fragmentList) {
            if (fragment is BaseFragment) {
                handled = fragment.onBackPressed()
                if (handled) {
                    break
                }
            }
        }
        if (!handled) {
            super.onBackPressed()
        }
    }
}
