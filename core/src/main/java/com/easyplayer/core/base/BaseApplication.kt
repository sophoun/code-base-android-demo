package com.easyplayer.core.base

import android.app.Application
import com.easyplayer.core.BuildConfig
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.squareup.leakcanary.LeakCanary
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

abstract class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // Initial Koin Dependency Injection
        startKoin {
            androidContext(applicationContext)
            modules(koinModules())
        }
        // Initial Leakcanary
        if (!LeakCanary.isInAnalyzerProcess(applicationContext)) {
            LeakCanary.install(this)
        }
        // Initial Logger library
        Logger.addLogAdapter(object : AndroidLogAdapter() {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
    }

    abstract fun koinModules(): List<Module>
}
