package com.easyplayer.core.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    val baseActivity: BaseActivity by lazy { activity as BaseActivity }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(layout(), container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    @LayoutRes abstract fun layout(): Int

    open fun onBackPressed(): Boolean {
        return false
    }
}
