package com.easyplayer.codebaseexample;

/**
 * This is sample class
 */
public class Person {
    private String name;
    private int age;

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * set name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * set age
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }
}