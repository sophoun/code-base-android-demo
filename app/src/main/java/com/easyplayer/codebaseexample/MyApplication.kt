package com.easyplayer.codebaseexample

import com.easyplayer.core.base.BaseApplication
import org.koin.core.module.Module
import org.koin.dsl.module

class MyApplication : BaseApplication() {
    override fun koinModules(): List<Module> = listOf(
        module {
        }
    )
}
