package com.easyplayer.codebaseexample

import com.easyplayer.core.base.BaseActivity

class MainActivity : BaseActivity() {
    override fun layout(): Int = R.layout.activity_main
}
